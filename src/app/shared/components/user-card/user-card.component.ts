import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interface/user';
import { cardsUser } from '../../../db/current-user';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  public user: User = cardsUser[0];

  constructor() { }

  ngOnInit(): void {} 

}
