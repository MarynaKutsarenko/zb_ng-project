import { Component, OnInit } from '@angular/core';
import { Slide } from '../../../interface/slider';
import { gallery } from '../../../db/slider';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CardInfoService } from 'src/app/services/send-info.service';
import { Card } from 'src/app/interface/card';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  public slidesStore: Slide[] = gallery;
  public dataInfo: Card = {} as Card;

  public customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: true,
    autoplay: true,
    startPosition: 3,
    autoWidth: true,
    dots: true,
    navSpeed: 1000,
    responsive: {
      940: {
        items: 5
      },
      1200: {
        items: 8
      }
    },
    nav: true
  }



  constructor( private cardService: CardInfoService ) {}

  ngOnInit(): void {
    this.cardService.subject.subscribe(data => {
      if (data) {
        this.dataInfo = data; 
      }
    });
  }

}
