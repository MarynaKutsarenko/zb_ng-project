import { Component, Input, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Filter } from '../../../interface/categories';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, AfterViewInit {
  public isActive: boolean = false;

  @Input() item: Filter = {} as Filter;
  
  @ViewChild('navdrop') navdrop!: ElementRef;
  @ViewChild('forIco') forIco!: ElementRef;

  constructor() { }

  public dropMenu() {
    this.getImage();
    this.isActive = !this.isActive;
    this.navdrop?.nativeElement.classList.toggle('_opened');
  } 

  public getImage() : void{
    if (!this.isActive) {
      if (this.forIco.nativeElement.classList.contains('other')) {
        this.forIco.nativeElement.classList.remove('other');
        this.forIco.nativeElement.classList.add('_active_other-img');
      } else {
        this.forIco.nativeElement.classList.remove('electronics');
        this.forIco.nativeElement.classList.add('_active_elec-img');
      }
    } else {
      if (this.forIco.nativeElement.classList.contains('_active_elec-img')) {
        this.forIco.nativeElement.classList.remove('_active_elec-img');
        this.forIco.nativeElement.classList.add('electronics');
      } else {
        this.forIco.nativeElement.classList.remove('_active_other-img');
        this.forIco.nativeElement.classList.add('other');
      }
    }
  }

  ngOnInit(): void { 
  }

  ngAfterViewInit() { }

}
