import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Card } from 'src/app/interface/card';
import { CardInfoService } from 'src/app/services/send-info.service';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit, OnDestroy {
  public itemCard$: Card = {} as Card;
  public isOpen: boolean = false;
  public date: number = Date.now();


  @ViewChild('showInfo') showInfo!: ElementRef;

  constructor( private cardService: CardInfoService ) { }

  public isShowInfo(): void {
    this.showInfo.nativeElement.classList.toggle('_opened');
  }

  ngOnInit(): void {
    this.cardService.subject.subscribe(data => {
      if (data) {
        this.itemCard$ = data; 
      }
    });

    setInterval(() => {
      this.date = Date.now()
    }, 1000);

  }

  ngOnDestroy(): void {  }

}
