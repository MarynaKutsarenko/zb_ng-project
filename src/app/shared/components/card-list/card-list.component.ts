import { Component, OnInit } from '@angular/core';
import { Card } from '../../../interface/card';
import { cards } from '../../../db/current-cards';
import { CardInfoService } from '../../../services/send-info.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
  
export class CardListComponent implements OnInit {
  public cards: Card[] = cards;
  public item: Card = {} as Card;
  public page: number = 2;
  public isOpened: boolean = false;
  private cardId!: string | null;


  constructor(
    private cardService: CardInfoService,
    public router: Router,
    private activateRoute: ActivatedRoute
  ) { }


  private getInfo(item: Card): void {
    this.cardService.subject.next(item);
  }

  public goToItemPage(card: Card, id: number): void {
    this.router.navigate([`good/${card.id}`]);
    this.getInfo(card);
  }

  ngOnInit(): void { }

}

