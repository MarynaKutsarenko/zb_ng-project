import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-btn-list',
  templateUrl: './btn-list.component.html',
  styleUrls: ['./btn-list.component.scss']
})
export class BtnListComponent implements OnInit {
  public isClicked: boolean = false;

  constructor() { }

  @ViewChild('menudrop') menudrop!: ElementRef;

  public showMenu() {
    this.isClicked = !this.isClicked;
    this.menudrop?.nativeElement.classList.toggle('_opened');
  }

  ngOnInit(): void {
  }

}
