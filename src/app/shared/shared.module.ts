import { NgModule, NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { FilterListComponent } from './components/filter/filter-list.component';
import { CardListComponent } from './components/card-list/card-list.component';
import { CardComponent } from './components/card-list/_card/card.component';
import { CategoriesListComponent } from '../shared/components/categories-list/categories-list.component';
import { ItemComponent } from './components/item/item.component';
import { SelectComponent } from '../shared/components/filter/_select/select.component';
import { ItemCardComponent } from './components/item-card/item-card.component';
import { SliderComponent } from './components/slider/slider.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UserCardComponent } from './components/user-card/user-card.component';
import { BtnListComponent } from './components/btn-menu/btn-list.component';

@NgModule({
  declarations: [
    FilterListComponent,
    CardListComponent,
    CardComponent,
    CategoriesListComponent,
    ItemComponent,
    SelectComponent,
    ItemCardComponent,
    SliderComponent,
    UserCardComponent,
    BtnListComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    FontAwesomeModule,
    CarouselModule,
  ],
  exports: [
    SelectComponent,
    SliderComponent,
    ItemCardComponent,
    UserCardComponent,
    FilterListComponent,
    CardComponent,
    CardListComponent,
    CategoriesListComponent,
    BtnListComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule { }
