import { Component, OnInit } from '@angular/core';
import { headerFilters } from '../../db/header-select';
import { Select } from '../../interface/select';
import { headerLanguages } from '../../db/header-language';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public headerOptions: Select[] = headerFilters;
  public languages: Select[] = headerLanguages;


  constructor() { }

  ngOnInit(): void {
  }

}
