import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CardInfoService {
  
  public subject: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor() { }
}
