export const headerLanguages = [
  {
    "title": "choose language",
    "options": [
      { title: 'English' }, 
      { title: 'Украiнська' },
      { title: 'Русский' }, 
      { title: 'Polska' }, 
    ],
    "type": "language"
  }
]