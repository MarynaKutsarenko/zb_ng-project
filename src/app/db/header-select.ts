export const headerFilters = [
  {
    "title": "SELL & BUY",
    "options": [
      { title: "seller"},
      { title: "buyer"},
    ],
    "image": "../../assets/images/header-img/sall.svg",
    "type": "sells" 
  },
  {
    "title": "CATALOGUE",
    "options": [
      { title: "CD & DVD"},
      { title: "Musical instruments"},
    ],
    "image": "../../assets/images/header-img/catalog.svg",
    "type": "allStuf" 
  },
]