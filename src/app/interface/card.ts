export interface Card {
   "id": number,
   "image": string,
   "price": number,
   "title": string,
   "description": string,
   "info": string,
   "options"?: Array<string>
}
