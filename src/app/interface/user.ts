export interface User {
  "id": number,
  "avatar": string,
  "address": string,
  "name": string,
  "description": string,
  "watchers": number 
}
