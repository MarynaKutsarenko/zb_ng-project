export interface Slide {
  "id": number,
  "image": string ,
  "description"?: string
}