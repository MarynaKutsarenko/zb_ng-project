import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.scss']
})
export class GoodsComponent implements OnInit {
  private cardId!: number | null;

  constructor( private activateRoute: ActivatedRoute ) { }

  ngOnInit(): void {
    this.cardId = Number(this.activateRoute.snapshot.paramMap.get('id'));
  }

}
