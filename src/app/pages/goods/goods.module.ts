import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoodsRoutingModule } from './goods-routing.module';
import { GoodsComponent } from './goods.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    GoodsComponent
  ],
  imports: [
    CommonModule,
    GoodsRoutingModule,
    SharedModule,
  ],
  exports: [
    GoodsComponent
  ]
})
export class GoodsModule { }
